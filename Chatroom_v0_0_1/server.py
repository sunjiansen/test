#!/usr/bin/python
# -*- coding:utf-8 -*-


import socket
import threading


BUFFERSIZE = 1024
existroom = {}


class Room:
    def __init__(self, name):
        self.name = name
        self.people = []

    def addpeople(self, address, name, con):
        self.people.append({'address':address, 'name':name, 'con':con})
        sendroom(self.name, "", name + "进入了房间")
        print(name + " enter")

    def outpeople(self,con):
        for i in range(len(self.people)):
            if self.people[i]["con"] == con:
                leavename = self.people[i]["name"]
                print(self.people[i]["name"] + " out")
                self.people.pop(i)
        sendroom(self.name, "", leavename + "离开了房间")



def Connect(sc):
    while True:
        try:
            con, address = sc.accept()
            datafrom = str(address[0]) + str(address[1])
            print(datafrom + " is connect")
            con.sendto(bytes("连接成功", encoding="utf-8"), address)
            threading.Thread(target=Recevice, args=(con, datafrom,address)).start()
        except:
            print("connect error")


def createroomoraddpeople(con,roomname,peoplename,address,data):
    if roomname not in existroom:
        existroom[roomname]= Room(roomname)
        existroom[roomname].addpeople(address,peoplename,con)
    else:
        existpeople = False
        for v in existroom[roomname].people:
            if v["name"] == peoplename:
                existpeople = True
        if existpeople == False:
            existroom[roomname].addpeople(address, peoplename,con)
        else:
            sendroom(roomname,peoplename,data)



def sendroom(roomname,peoplename,data):
    for v in existroom[roomname].people:
        senddata = peoplename + "：" + data
        v["con"].sendto(bytes(senddata, encoding="utf-8"), v["address"])


def Recevice(con, datafrom,address):
    run = True
    while run:
        try:
            receivedata = str(con.recv(BUFFERSIZE), encoding="utf-8")
            try:
                rdata = receivedata.split(':')
                createroomoraddpeople(con,rdata[0],rdata[1],address,rdata[2])
            except:
                con.sendto(bytes("发送数据格式错误", encoding="utf-8"), address)
        except:
            for k in existroom:
                existroom[k].outpeople(con)
            run = False


sc = socket.socket()

sc.bind(("127.0.0.1", 8005))
sc.listen(5)
print("server working")

threading.Thread(target=Connect,args=(sc,)).start()

import unittest
import socket
import json
import datetime

HOST = ('127.0.0.1', 8000)

class MyTestCase(unittest.TestCase):
    # def setUp(self):
    #     print('start')
    #
    # def tearDown(self):
    #     print('end')
    connectlist = {}

    def Request(self,opertion, msg, receiver):
        response = {'operation': opertion, 'msg': msg, 'receiver': receiver}
        return json.dumps(response)

    @classmethod
    def tearDownClass(cls):
        if cls.connectlist:
            for i in range(len(cls.connectlist)):
                cls.connectlist[i].close()

    @classmethod
    def setUpClass(cls):
        print('请输入你想连接的数量：')
        if not cls.connectlist:
            number = input()
            while not number.isdigit():
                print('请输入数字：')
                number = input()
            for i in range(0,int(number)):
                sc = socket.socket()
                sc.connect(HOST)
                cls.connectlist[i] = sc
        else:
            print(cls.connectlist)

    def test_function(self):
        #自我校验
        for i in range(len(self.connectlist)):
            request = self.Request('reback', 'ok', 'x')
            self.connectlist[i].send(bytes(request, encoding='utf-8'))
            receivedata = str(self.connectlist[i].recv(1024), encoding='utf-8')
            try:
                self.assertEqual(receivedata, 'ok')
            except:
                print(receivedata, 'ok', '发送接受信息不一致')

        #发送给某人(测试自己)
        for i in range(len(self.connectlist)):
            request = self.Request('send', 'ok', 'self')
            self.connectlist[i].send(bytes(request, encoding='utf-8'))
            receivedata = str(self.connectlist[i].recv(1024), encoding='utf-8')
            try:
                self.assertEqual(receivedata, 'ok')
            except:
                print(receivedata, 'ok', '发送接受信息不一致')

        #发送全部人
        receiver = self.connectlist[0]
        for i in range(len(self.connectlist)):
            request = self.Request('sendall', 'ok', '')
            self.connectlist[i].send(bytes(request, encoding='utf-8'))
            receivedata = str(receiver.recv(1024), encoding='utf-8')
            try:
                self.assertEqual(receivedata, 'ok')
            except:
                print(receivedata, 'ok', '发送接受信息不一致')

    #压测
    def test_ab(self):
        print('请输入你想发的请求数量：')
        number = input()
        while not number.isdigit():
            print('请输入数字：')
            number = input()
        start = datetime.datetime.now()
        for i in range(0, int(number)):
            request = self.Request('reback', 'ok', 'x')
            self.connectlist[0].send(bytes(request, encoding='utf-8'))
            receivedata = str(self.connectlist[0].recv(1024), encoding='utf-8')
            try:
                self.assertEqual(receivedata, 'ok')
            except:
                print(receivedata, 'ok', '发送接受信息不一致')
        end = datetime.datetime.now()
        print('用时：{0}'.format(end-start))


if __name__ == '__main__':
    unittest.main()

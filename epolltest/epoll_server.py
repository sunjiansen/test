#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: sunjiansen on :


import socket
import select
import threading
import queue
import json

connecttions = {}
request = {}
textupd = 'this is a upd data'
command = 0


#定义返回数据和进入消息队列

def MyResponse(dealoption, fileno):
    data = json.loads(dealoption)
    if 'operation' not in data \
            or 'msg' not in data or 'receiver' not in data:
        response = {'operation': 'send', 'msg': '错误发送格式',
                    'receiver': fileno, 'sender': fileno}
    else:
        response = {'operation': data['operation'], 'msg': data['msg'],
                    'receiver': data['receiver'], 'sender': fileno}
    q.put(response)

#epoll模块进行非阻塞io连接接受数据

def epolldeal():
    events = epoll.poll()
    for fileno, event in events:
        if fileno == sc.fileno():
            con, addr = sc.accept()
            confd = con.fileno()
            epoll.register(confd, select.EPOLLIN)   # 这里必须要int，后面操作要注意
            connecttions[confd] = con
        elif event & select.EPOLLHUP:
            epoll.unregister(fileno)
            connecttions[fileno].close()
            del connecttions[fileno]
        elif event & select.EPOLLIN:
            request[fileno] = str(connecttions[fileno].recv(1024), encoding='utf-8')
            if request[fileno] == '':
                epoll.unregister(fileno)
                connecttions[fileno].close()
                del connecttions[fileno]
            else:
                MyResponse(request[fileno],fileno)
        # 改用消息队列后这部分其实没什么用
        elif event & select.EPOLLOUT:
            connecttions[fileno].send(bytes(request[fileno], encoding='utf-8'))
            MyResponse(request[fileno],fileno)
            epoll.modify(fileno, select.EPOLLIN)

# 服务器小广播和简单数据统计
def Udp():
    check = []
    right = []
    error = []
    for i in connecttions:
        try:
            connecttions[i].send(bytes(textupd, encoding='utf-8'))
            right.append({'fileno':i})
        except:
            error.append({'fileno':i})
    try:
        rate = len(right)/len(right)+len(error)
    except ZeroDivisionError:
        rate = 1e99
    check.append({'right': right, 'error': error,
                  'rightnumber': len(right), 'errornumber': len(error),
                  'rate': rate})
    print(check)


# 展示已有的连接表
def ShowList():
    print(connecttions)


# 拓展功能
def zero():
    print('this is 0')

# 服务端命令处理器
def commandinput():
    while True:
        commandlist = {'0': zero,'1': Udp,'2': ShowList}
        command = input('请输入命令:')
        if command in commandlist:
            commandlist[command]()
        else:
            print('错误指令')

# 客服端想进行的操作
# 1、发送给某人
# 2、发送全部
# 3、自我校验
def send(dealoption):
    print('send')
    # response = json.loads(dealoption)
    response = dealoption
    if response['receiver'] == 'self':
        connecttions[response['sender']].send(bytes('ok', encoding='utf-8'))
    else:
        try:
            int(response['receiver'])
            if int(response['receiver']) not in connecttions.keys():
                connecttions[response['sender']].send(bytes('您要发送的人已经断线', encoding='utf-8'))
            else:
                senddata = response['receiver'] + response['msg']
                connecttions[int(response['receiver'])].send(bytes(senddata, encoding='utf-8'))
                connecttions[response['sender']].send(bytes('发送成功', encoding='utf-8'))
        except:
            connecttions[response['sender']].send(bytes('请输入数字', encoding='utf-8'))


def sendall(dealoption):
    response = dealoption
    for i in connecttions:
        print(connecttions[i])
        connecttions[i].send(bytes(response['msg'], encoding='utf-8'))


def reback(dealoption):
    response = dealoption
    print( connecttions[response['sender']])
    print(type(response['msg']))
    connecttions[response['sender']].send(bytes(response['msg'], encoding='utf-8'))



# 采用字典对应命令的组合方便以后命令的拓展，不用过多的if、else
def queuedeal():
    queuecomlist = {'send': send, 'sendall': sendall, 'reback': reback}
    while not q.empty():
        dealoption = q.get()
        if dealoption['operation'] in queuecomlist.keys():
            queuecomlist[dealoption['operation']](dealoption)
        else:
            connecttions[dealoption['sender']].send(bytes('无该项操作', encoding='utf-8'))

#   程序开始
#   PF_INET, AF_INET： Ipv4网络协议；
#   PF_INET6, AF_INET6： Ipv6网络协议。
#   type参数的作用是设置通信的协议类型，可能的取值如下所示：
#   SOCK_STREAM： 提供面向连接的稳定数据传输，即TCP协议。
#   OOB： 在所有数据传送前必须使用connect()来建立连接状态。
#   SOCK_DGRAM： 使用不连续不可靠的数据包连接。
#   SOCK_SEQPACKET： 提供连续可靠的数据包连接。
#   SOCK_RAW： 提供原始网络协议存取。
#   SOCK_RDM： 提供可靠的数据包连接。
#   SOCK_PACKET： 与网络驱动程序直接通信
sc = socket.socket()  #默认是AF_INET + SOCK_STREAM

#sc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #复用ip

sc.bind(('127.0.0.1',8000))
sc.listen(10)

epoll = select.epoll()
#当对端有数据写入粗发
epoll.register(sc.fileno(), select.EPOLLIN)

# print(sc.fileno())

threading.Thread(target=commandinput).start()     #服务端接受命令
# threading.Thread(target=queuedeal).start()        #消息队列处理
q = queue.Queue()

while True:
    # epoll 处理
    epolldeal()
    queuedeal()
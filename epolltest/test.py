#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: sunjiansen on :

import socket
import json


host = ('127.0.0.1', 8000)
testnumber = input('请输入你要测试的连接数量：')
print('开始创建socket')
socketlist = {}
for i in range(0,int(testnumber)):
    try:
        sc = socket.socket()
        print(str(i) + '创建成功')
        socketlist[i] = sc
    except:
        print(str(i) + '创建失败')
print('开始连接')
for i in socketlist:
    try:
        socketlist[i].connect(host)
        print(str(i) + '连接成功')
    except:
        print(str(i) + '连接失败')

print('开始测试')
print('开始发送接收测试')
#封装一个输出数据格式
def Response(opertion, msg, receiver):
    response = {'operation':opertion, 'msg':msg, 'receiver':receiver}
    return json.dumps(response)

for i in socketlist:
    try:
        socketlist[i].send(bytes(Response(opertion='reback', msg=str(i), receiver=''), encoding='utf-8'))
        print(str(i) + '发送成功')
        try:
            data = str(socketlist[i].recv(1024), encoding='utf-8')
            if data == str(i):
                print('返回数据一致')
            else:
                print('返回数据为：' + data)
                print('返回数据不一致')
            print(str(i) + '接收成功')
        except:
            print(str(i) + '接收失败')
    except:
        print(str(i) + '发送失败')

for i in socketlist:
    try:
        socketlist[i].close()
        print('断开成功')
    except:
        print('断开失败')

